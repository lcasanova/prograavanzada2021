package Simulador;

public class Simulacion {
	
	private Comunidad prueba;
	  
	Simulacion () {}
		
	public void addComunidad (Comunidad prueba) {
		this.prueba = prueba;
	}
	

	public void run (int steps) {
		prueba.Info_inicial();
		prueba.E_poblacion();
		prueba.Gravedad_c_enfermedadd();
		
		for (int i = 1; i < steps +1; i++) {
			
			prueba.informacion(i);
			if (prueba.getEnfermedad().getCasosactivos() != 0) {
				prueba.paso();
			}
			
		}
		prueba.C_muertos();
		prueba.C_recuperados();
	}
	
	
}
