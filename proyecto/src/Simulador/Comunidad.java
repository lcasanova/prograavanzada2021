package Simulador;
import java.util.ArrayList;
import Enfermedad.*;
import java.util.Collections;


public class Comunidad {
	
	private int Contacto;
	private Double pro_contacto;
	private Virus enfermedad;	
	private int poblacion;
	private int infectado;
	private int Total_Infectados;	
	private int edadc;
	
	private ArrayList<Persona> Poblacion = new ArrayList<Persona>();
	private ArrayList<E_base> Base  = new ArrayList<E_base>();
    private ArrayList<E_afecciones> Afecciones = new ArrayList<E_afecciones>();
	
	Comunidad (int poblacion, int infectado, int Contacto, Double pro_contacto, Virus enfermedad) {
		
		this.poblacion = poblacion;
		this.infectado = infectado;
		this.Total_Infectados = infectado;
		this.Contacto = Contacto;
		this.pro_contacto = pro_contacto;
		this.enfermedad = enfermedad;
	
	}
	
	public ArrayList<Persona> getPoblacion() {
		return Poblacion;
	}
	
	public Virus getEnfermedad () {
		return this.enfermedad;
	}
	
	public void poblacion_inicial () {
		
		for (int i=1; i < this.poblacion+1; i++) {
			
			Poblacion.add(new Persona(this.enfermedad));
			Poblacion.get(i-1).setId(i);
			edadc = (int)(Math.random()*85+1);
			Poblacion.get(i-1).setEdad(edadc);
			
		}
			
		for (int j=1; j <= this.infectado; j++) {
			
			Boolean contagio = false;		
			while(contagio == false) {
		
				int subject = (int)(Math.random()*this.poblacion);
										
				if (Poblacion.get(subject).getStatus() != "Infectado") {					
				
					Poblacion.get(subject).setStatus("Infectado");
					this.enfermedad.addCaso_activo();
					contagio = true;
					
				}
			}
		}
	}
	
	
	public ArrayList<E_base> getBase() {
		return Base;
	}


	public void setE_bases(E_base b) {
		Base.add(b);
	}


	public ArrayList<E_afecciones> getAfecciones() {
		return Afecciones;
	}


	public void setAfecciones(E_afecciones a) {
		Afecciones.add(a);
	}
	
	public void paso () {

		Double Contacto = (Math.random()*1);
		if (Contacto <= pro_contacto) {
					
			for (int i = 0; i < this.Contacto; i++) {
			
				Boolean infectar = false;
				while (infectar == false) {
							
					
					int persona_x = (int)(Math.random()*this.poblacion);
					
										
					if (Poblacion.get(persona_x).getStatus() != "Infectado") {
						
																
						if (this.enfermedad.accion_infectar(Poblacion.get(persona_x))) {											
							Poblacion.get(persona_x).setStatus("Infectado");					
								this.enfermedad.addCaso_activo();
								
							}
							infectar = true;					
					}
				}
			}		
		}
		
		
		for (int i = 0; i < Poblacion.size(); i++) {
			Poblacion.get(i).comprobar();
		}
	}

	public void E_poblacion() {
		
		Collections.shuffle(getPoblacion());		
		double Poblacion_E_base = getPoblacion().size()*0.25;
		for(int b = 0; b < Poblacion_E_base;b++) {
			int E_Base_random = (int)(Math.random()*5);
			getPoblacion().get(b).setBase(Base.get(E_Base_random));
		}
		
		Collections.shuffle(getPoblacion());
		double Poblacion_Afeccion = getPoblacion().size() *0.65;
		for(int a = 0; a< Poblacion_Afeccion; a++) {
			int Afeccion_random = (int)(Math.random()*2);
			getPoblacion().get(a).setAfeccion(Afecciones.get(Afeccion_random));   
		}
	}

	
	public void Info_inicial() {
		 
		int edad_total = 0;
		double promedio = 0.0;
		for(Persona i: getPoblacion()){
			edad_total = edad_total + i.getEdad();
		}
		
		promedio = edad_total/getPoblacion().size();
		System.out.println("total personas:" + getPoblacion().size());	
		System.out.println("Poblacion con enfermedad base: " + (int)(getPoblacion().size()*0.25));
		System.out.println("Poblacion con Afeccion: " +(int)(getPoblacion().size()*0.65));
		System.out.println("El promedio de edad de la comunidad es: " + promedio);
		System.out.println("\n");
		
	}

	public void C_muertos() {
		int cont = 0;
		int resta;

		for(Persona p: getPoblacion()) {
			if(p.getStatus() == "Muerto") {
				cont++;			
			}	
		}
		
		for(int b = 0; b < getPoblacion().size();b++) {
			if(getPoblacion().get(b).getStatus() == "Muerto"){
				Poblacion.remove(b);
			}
		}
		
		resta = getPoblacion().size();
		System.out.println("");
		System.out.println("La cantidad de muertos es de : "+cont+" personas");
		System.out.println("total personas:" + resta);	
	}

	public void C_recuperados() {
		int cont = 0;
		for(Persona p: getPoblacion()) {
			if(p.getStatus() == "Sano") {
				cont++;
			}	
		}
		
		int edad_total = 0;
		double promedio = 0.0;
		for(int a = 0; a< getPoblacion().size(); a++) {
			edad_total = edad_total + getPoblacion().get(a).getEdad();
		}
		promedio = edad_total/getPoblacion().size();
		System.out.println("El promedio de edad de la comunidad es: " + promedio);
		System.out.println("La cantidad de recuperados es de : "+cont+" personas");
		System.out.print("Total de contagios: "+this.Total_Infectados);
	}
	
	public void Gravedad_c_enfermedadd() {
		
		for(Persona p: getPoblacion()) {
			double indice = 0.0;
			for(E_afecciones a: p.getAfeccion()) {
				indice = a.Getprobabilidad();
			}
			for(E_base b: p.getE_base()) {
				indice += b.Getprobabilidad();
			}
			
			int p_g_s = (int) (Math.random()*10+1); // 60% de no estar grave si esta sano
			if(p_g_s >= 1 || p_g_s >=4) {
				indice += (Math.random()*1);
			}
					
			p.Setgravedad(indice);
		}
	}
	
	
	int k;
	
	public void informacion (int step) {
		
		this.Total_Infectados = this.enfermedad.getCasosactivos();
		if(k<this.enfermedad.getCasosactivos()) {
	            this.Total_Infectados = this.enfermedad.getCasosactivos();
	        }else { this.Total_Infectados=k;}
		System.out.print("[Paso "+step+"] ");
		System.out.print("Total de contagios de la comunidad 1: "+this.Total_Infectados );
		System.out.print(" Casos activos: "+this.enfermedad.getCasosactivos());
	    k=this.Total_Infectados;
	    System.out.println("");
	   
	}
	
	// todo relaciona con Vacunas
	
  

		
	
}
