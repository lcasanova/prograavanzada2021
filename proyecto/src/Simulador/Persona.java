package Simulador;
import java.util.ArrayList;
import Enfermedad.*;

public class Persona {
	
	private Virus disease;
	private int id = 0;
	private int edad ;
	private String status = "Sano";	
	private int tiempo = 0;
	private Double probabilidad_de_muerte = 0.0;
	private double gravedad = 0.0;
	
	private ArrayList<E_base> bases = new ArrayList<E_base>();
	private ArrayList<E_afecciones> afeccion = new ArrayList<E_afecciones>();
	
	Persona (Virus disease) {
		this.disease = disease;
	}
	
	public int getEdad () {
		return this.edad;
	}
	
	public void setEdad (int edad) {
		this.edad = edad;
	}
	
	public void setId (int id) {
		this.id = id;
	}
	
	public int getId () {
		return this.id;
	}
	
	public void setStatus (String status) {
		this.status = status;
	}
	
	public String getStatus () {
		return this.status;
	}
	
	public int getDia_infectado () {
		return this.tiempo;
	}
	
	public void Setgravedad(double gravedad) {
		this.gravedad = gravedad;	
	}
	
	public double Getgravedad() {
		return gravedad;
	}
	
	public void setBase(E_base b) {
		bases.add(b);
	}
	
	public ArrayList<E_base> getE_base() {
		return bases;
	}
	
	public void setAfeccion(E_afecciones a) {
		afeccion.add(a);
	}
	
	public ArrayList<E_afecciones> getAfeccion() {
		return afeccion;
	}
	
	public void comprobar() {
	
			if (this.status == "Infectado") {
			
		
				if (this.tiempo < disease.getInfeccion()) {
					int estado_del_enfermo = (int)(Math.random()*2+1);
								
		
				if (estado_del_enfermo== 1) {
					this.probabilidad_de_muerte += this.disease.getMuertes();
				}
				
				 
				if ((Math.random()*1) < probabilidad_de_muerte) {
					this.setStatus("Muerto");
					this.disease.eliminar_caso();
				}			
				
				this.tiempo += 1;
				
				if(Getgravedad() <= 1) {
					this.setStatus("Sano");			
					this.disease.eliminar_caso();				
				
				if(this.tiempo >= this.disease.getInfeccion()*2/3) {
					if(Getgravedad()>1) {
						this.setStatus("Muerto");
						this.disease.eliminar_caso();
					}else {
						if(getEdad() > 70 || getEdad() < 7) {
							this.setStatus("Muerto");
							this.disease.eliminar_caso();	
						}else {
							this.setStatus("Sano");			
							this.disease.eliminar_caso();
							
						}
					}
				}
			}
				
			
		}
			
		else if (this.tiempo >= this.disease.getInfeccion()) {
			this.setStatus("Sano");			
			this.disease.eliminar_caso();
		}
				
		
		}
		
	}
}
