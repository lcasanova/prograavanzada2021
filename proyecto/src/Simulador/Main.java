package Simulador;
import Enfermedad.*;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
					
		Virus enfermedad = new Virus(0.3, 15, 0.15);	
		Comunidad c = new Comunidad(20000, 10, 5000, 0.9, enfermedad);
		c.poblacion_inicial();
		E_base a,e,f,h,p;
		E_afecciones o,d;
		
		// Enferemedades base
		a = new E_base("Asma",0.6);
		e = new E_base("Enfermedad cerebro vascular",0.4);
		f = new E_base("Fibrosis quistica",0.3);
		h = new E_base("Hipertension",0.5);
		p = new E_base("Presion alrterial alta",0.2);
				
		// Afecciones
		o = new E_afecciones("Obesidad",0.5);
		d = new E_afecciones("Desnutricion",0.3);
		
		// agregar enfermedades
		c.setE_bases(a);;
		c.setE_bases(e);
		c.setE_bases(f);
		c.setE_bases(h);
		c.setE_bases(p);
				
		c.setAfecciones(d);
		c.setAfecciones(o);
		
		Simulacion simulation = new Simulacion();
		        		    
		simulation.addComunidad(c);
		simulation.run(40);
		
		
			
	
	}
	
	 
}
