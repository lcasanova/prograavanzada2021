package Simulador;
public class Virus {
		
	private int pasos_promedio;
	private int Casos_activos;
	private Double infeccion;
	private Double muertes;
	private Boolean Resultados_infeccion;
	
	Virus (Double infeccion, int pasos_infeccion, Double muerte) {
		this.infeccion = infeccion;
		this.pasos_promedio = pasos_infeccion;
		this.muertes = muerte/pasos_infeccion;
	}	

	public int getInfeccion () {
		return this.pasos_promedio;
	}
	
	public void addCaso_activo () {
		this.Casos_activos += 1;
	}
	
	public int getCasosactivos () {
		return this.Casos_activos;
	}
	
	public Double getMuertes () {
		return this.muertes;
	}
	
	public void eliminar_caso () {
		this.Casos_activos += -1;
	}	
	
	public Boolean accion_infectar (Persona subject) {
		
		if (Math.random()*1 <= this.infeccion) {
			Resultados_infeccion = true;
		} else {
			Resultados_infeccion = false;
		}
		return Resultados_infeccion;
	}
	
}
