
public class Estanque {

	private Double volumen = 32.0;
	private Double menos = 0.0;

	public Double getVolumen() {
		return volumen;
	}

	public void setVolumen(Double volumen) {
		this.volumen = volumen;
	}
	public void setmenos(Double menos) {
		if (this.volumen <= 0) {
			System.exit(0);
		}
		this.menos = menos;
		this.volumen = this.volumen - (this.volumen*this.menos);
	}

}
