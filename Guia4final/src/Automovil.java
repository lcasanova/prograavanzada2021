
import java.util.ArrayList;
import java.util.Random;

public class Automovil {

	private Motor motor;
	private Velocimetro velocimetro;
	private ArrayList<Rueda> rueda;
	private Estanque estanque;
	private Boolean encendido ;
	double dist;
	double recorrido;
	double TiempoTotal;
	private float Tiempo;
	private int cambio_rueda;

	Automovil(){
		this.rueda = new ArrayList<Rueda>();
		this.encendido = false;
	}
	
	public Motor getMotor() {
		return motor;
	}
	
	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public Velocimetro getVelocimetro() {
		return velocimetro;
	}

	public void setVelocimetro(Velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}

	public Estanque getEstanque() {
		return estanque;
	}

	public void setEstanque(Estanque estanque) {
		this.estanque = estanque;
	}

	public Boolean getEncendido() {
		return encendido;

	}

	public void setEncendido(Boolean encendido) {
		this.encendido = encendido;
	}

	public ArrayList<Rueda> getRueda() {
		return rueda;
	}

	public void setRueda(Rueda rueda) {
		this.rueda.add(rueda);
	}
	
	public void apaga_automovil() {
		this.velocimetro.setVelocidad(0.0);
		this.encendido = false;
	}
	
	public void enciende_automovil() {
		
		this.encendido = true;
		
	}
	
	public void movimiento() {
		Random ran = new Random();
		float tiempo = ran.nextFloat()*9+1;
	    this.Tiempo = tiempo; 
		this.TiempoTotal = TiempoTotal + tiempo;
		this.recorrido = this.velocimetro.getVelocidad()*(tiempo/3600);
		this.dist = dist + recorrido;

		System.out.println("El auto se mueve");
	}
	
	public void chequeo_ruedas() {
		int k=0;
		for (Rueda r : this.rueda) {
			k = k + 1;
			if (10 >= r.getDesgaste()) {
				r.cambiorueda();
				System.out.println("Rueda " + k + " ya se ha cambiado");
				this.cambio_rueda = this.cambio_rueda + 1;

			System.out.println("La rueda est� al "+ k + r.getDesgaste());
		}}
	}
	public void gasto_litros() {
		
		if (this.motor.getCilindrada()=="1,2") {
			this.estanque.setmenos(this.recorrido/20);
		}
		else {
			this.estanque.setmenos(this.recorrido/14);
		}
	}
	public void gasto_ruedas() {
		for (Rueda r : this.rueda) {
			Random porciento = new Random();
			int desgasteporciento = porciento.nextInt(10)+1;
			r.setDesgaste(desgasteporciento);
		}
	}

	
	public void reporte_automovil() {
		System.out.println("Las ruedas fueron cambiadas: " + this.cambio_rueda + " veces");
		System.out.println("El automovil tiene un motor de " + this.motor.getCilindrada());
		System.out.println("El estado actual del estanque es: " + this.estanque.getVolumen());
		chequeo_ruedas();
		System.out.println("La velocidad actual del automovil en km/h es: " + this.velocimetro.getVelocidad());
		if (this.getEncendido()==true) {
			System.out.println("Auto encendido correctamente");
		}
		else {
			System.out.println("Auto apagado");
		}
		if (this.getEncendido()==true) {
			System.out.println("El auto se mueve esta cantidad de km:" + recorrido + " en un tiempo de: " + Tiempo + "s");
			System.out.println("Kilometraje auto: " + dist + " en una cantidad de tiempo total de: " + TiempoTotal + "s");

		}
		else {
			System.out.println("El auto no se mueve");
		}
		
	}
	
}
