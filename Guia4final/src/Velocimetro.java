
public class Velocimetro {
	
	/* Velocidad refiere a velocidad actual del automovil */
	private int velocidad;
	private int velocidad_maxima = 120;
	
	
	public int getVelocidad() {
		return velocidad;
	}
	
	public void setVelocidad(double velocidadd) {
		this.velocidad = (int) velocidadd;
	}
	
	public int getVelocidadMaxima() {
		return this.velocidad_maxima;
	}
	

}
