import java.util.Scanner;

public class Controlador {

	Controlador(){
		
		Automovil a = new Automovil();
		
		/* valor por defecto */
		String cilindrada = "1.2";
		
		/* dos formas distintas de instanciar */
		//Motor m = new Motor(cilindrada);
		Motor m1 = new Motor();
		
		a.setMotor(m1);
		
		Velocimetro v = new Velocimetro();
		a.setVelocimetro(v);
		
		Estanque e = new Estanque();
		a.setEstanque(e);
		
		for (int i=0; i<4;i++) {
			a.setRueda(new Rueda());
		}
		
		//a.chequeo_ruedas();
		for (double i=0.0; i< a.getEstanque().getVolumen(); ) {
			a.reporte_automovil();
			try {
				
				System.out.println("Encienda auto con 0 y apague con cualquier otro bot�n");
				Scanner entrada = new Scanner(System.in);
				int interruptor = entrada.nextInt();
				if (interruptor == 0) {
					System.out.println("Escriba los Km/h en double del auto <=120Km/h ");
					Scanner entrada1 = new Scanner(System.in);
					double velocidadd = entrada1.nextDouble();
					if (velocidadd>120) {
						velocidadd=120;
					}
					a.getVelocimetro().setVelocidad(velocidadd);
				}
				if (interruptor == 0) {
					if (a.getEncendido()!=true) {
						a.enciende_automovil();
						a.getEstanque().setmenos(0.1);	
					}
					a.movimiento();
					a.gasto_litros();
					a.gasto_ruedas();
				}else{
					a.apaga_automovil();
				}
			}
			catch (Exception error) {
			}
		}
	}

		
	}

