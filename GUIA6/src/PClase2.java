import java.util.HashSet;
import java.util.Iterator;

import javax.swing.JOptionPane;

public class PClase2 {
	//Anotar:
	//estudiantes.remove para eliminar estudiante estudiantes.add para a�adir
	//addAll() unir 2 conjuntos del mismo tipo!! estudiantes.addAll(2doconjunto); luego imprimir estudiantes
	//retainAll() intersecci�n de 2 conjuntos, mismo orden de arriba, puede otorgar un resultado vac�o
	//en ambos casos el conjunto estudiantes quedar� alterado
	//containsAll() para saber si est� contenido en otro conjunto(su totalidad)
	//removeAll() diferencias entre conjuntos
PClase2(){
	HashSet<String> estudiantes=new HashSet<>();
		estudiantes.add("Lucas");
		estudiantes.add("Maxi");
		estudiantes.add("Alexis");
		estudiantes.add("Orlando");
		estudiantes.add("Sofia");
		//System.out.println(estudiantes);
	HashSet<String> estudiantescopia=new HashSet<>();
		estudiantescopia.addAll(estudiantes);
	Iterator<String> iterate=estudiantes.iterator();
	while(iterate.hasNext()) {
		System.out.println(iterate.next());	
	}	
	String alumno;
	boolean respuesta1;
	alumno= (JOptionPane.showInputDialog("Escriba el alumno a verificar: "));
	HashSet<String> alumnoParticular=new HashSet<>();
		alumnoParticular.add(alumno);
	respuesta1=estudiantes.containsAll(alumnoParticular);
	if (respuesta1==true){
		System.out.println("El estudiante indicado se encuentra en la clase.");
	}else { System.out.println("El estudiante indicado NO se encuentra en la clase.");}
	
	HashSet<String> estudiantes2020=new HashSet<>();
		estudiantes2020.add("Constanza");
		estudiantes2020.add("Alejandra");
		estudiantes2020.add("Joaquin");
		estudiantes2020.add("Maite");
		estudiantes2020.add("Luis");
		estudiantes2020.add("Lucas");
	boolean respuesta2;
	respuesta2=estudiantes.containsAll(estudiantes2020);
	if (respuesta2==true){
		System.out.println("La totalidad de estudiantes2020 se encuentra en estudiantes.");
	}else { System.out.println("La totalidad de estudiantes 2020 NO se encuentra.");}
	HashSet<String> estudiantescopia2=new HashSet<>();
		estudiantescopia2.addAll(estudiantes2020);
		estudiantescopia2.retainAll(estudiantes);
		estudiantescopia.addAll(estudiantes2020);
	System.out.println("Uni�n: " + estudiantescopia);
	estudiantescopia.removeAll(estudiantes2020);
	estudiantescopia.addAll(estudiantescopia2);
	System.out.println("con los estudiantes del 2020 ya eliminados: " + estudiantescopia);
	
	
	//System.out.println(estudiantes2); estudiantes2 pasa a ser estudiantescopia2
	//estudiantes2.addAll(estudiantes);
	
	//estudiantes2020.removeAll(estudiantes);
	//System.out.println("con los estudiantes del 2020 ya eliminados: " + estudiantes2020);



}
}
